const { colors } = require("tailwindcss/defaultTheme");
const { variants } = require("tailwindcss/stubs/defaultConfig.stub");

// See https://tailwindcss.com/docs/configuration for details
module.exports = {
  purge: ["./src/**/*.js"],
  // https://github.com/tailwindlabs/tailwindcss-forms
  plugins: [require("@tailwindcss/forms")],
  theme: {
    extend: {
      backgroundColor: theme => ({
        ...theme('colors'),
        'd-blue':'#000D51',
        'm-blue':'#011A62',
        'l-blue':'#1560F1',

      }),
      fontFamily: theme => ({
        'lexend-mega':["Lexend Mega","Sans-serif"],
        'lexend-deca':["Lexend Deca","Sans-serif"],

      }),
      colors:theme =>({
        'dark-blue':'#000D51',
        'mid-blue':'#011A62',
        'dark-pink':'#791153',

      }),
      boxShadow:theme=>({
        'xs':' 0 0 3px 0 rgba(0 , 0 , 0 , 0.15)'
      })
    },
    screens: {
      'mobile': {'max': '767px'},
      'tablet': {'min': '768px', 'max': '1023px'},
      'laptop': {'min': '1024px', 'max': '1279px'},
      'desktop': {'min': '1280px', 'max': '1535px'},
      'wide-desktop': {'min': '1536px'}
    }
  },
  variants: {
    extend :{
    backgroundColor: [ 'active'],
    ringWidth: ['hover', 'active'],
    ringColor: ['hover', 'active'],
    ringOpacity: ['hover', 'active'],
      transform: ['hover', 'focus']
    }
}
};
