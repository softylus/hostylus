import React ,{ useState }  from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Migration from "../components/migration";
import Blazing from "../components/blazing";
import Landing from "../components/landing";
import PlanCard from "../components/plan_card";
import LI from "../components/plan-li";
import ItemsCarousel from 'react-items-carousel';
import Slide from "../components/carouselSlide";
// import axios from 'axios'    cds;
// import qs from 'qs';
import { graphql } from "gatsby";
/* eslint react/prop-types: 0 */
var axios = require('axios');
      var qs = require('qs');



function IndexPage({data}) {
    const [activeItemIndex, setActiveItemIndex] = useState(0);
    const chevronWidth = 40;
    function getDomainResults(){

      var data = qs.stringify({
       'username': 'aseel',
      'password': '7ef6736f48c4b3422c100c22a25c60b6',
      'action': 'DomainWhois',
      'domain': 'hostylus.net' 
      });
      var config = {
        method: 'post',
        url: 'https://client.hostylus.com/includes/api.php' ,
        headers: { 
          'secret': '495nXGLH6hUScVDcYc25awaxkqDkt4wF', 
          'identifier': 'WQEjfG7BUWmxzVLWFgcG38MTq695TXIE', 
          'username': 'aseel', 
          'password': '7ef6736f48c4b3422c100c22a25c60b6', 
          'Access-Control-Allow-Origin': '*', 
          'Content-Type': 'application/x-www-form-urlencoded', 
        },
        data : data
      };
      
      axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        alert(JSON.stringify(response.data))
      })
      .catch(function (error) {
        console.log(error);
      });    }
  return (

    <Layout>
      <SEO
        keywords={[`Hostylus`, `Web Hosting`, `Hosting`, `Domain`, 'Domains', 'VPS', 'Wordpress Hosting']}
        title="Domain Names, Websites, Hosting & Online Marketing Tools - Hostylus"
        lang="en"
        image="hostylus-card-preview.jpg"
        description="Hostylus is a fast developing hosting service provider based in the United States, and serving customers from all around the globe. we aim to fast-expand our online presence."
      />
      
        

        <Landing image={data.landing.childImageSharp.fluid}  />
        
      

        <section className="max-w-5xl mx-auto my-16 mobile:pb-3">
            <h2 className="text-4xl text-black font-bold text-center">Choose Your Best Plan</h2>
            <h4 className="text-gray-400 text-center text-xl px-16 my-3">High Quality Plans in Fair Rate</h4>
            <div className="grid  grid-cols-4 gap-2 flex justify-items-center mt-6 mobile:grid-cols-1 tablet:grid-cols-2 tablet:justify-items-stretch py-8">
                <PlanCard planSVG={<img className="m-auto" src="https://ik.imagekit.io/softylus/business_hosting_Icon_zalIxHlA_q.svg"/>} header={"business hosting"} price={9.99} btnLink={"/Business"}
                          myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                              <LI LI={"100% SSD cloud hosting"} textSize={"text-sm"}/>
                              <LI LI={"Unlimited bandwidth"} textSize={"text-sm"}/>
                              <LI LI={"WordPress optimised"} textSize={"text-sm"}/>
                              <LI LI={"Free SSL certificates"} textSize={"text-sm"}/>
                              <LI LI={"Daily backups"} textSize={"text-sm"}/>
                              <LI LI={"FREE website builder"} textSize={"text-sm"}/>
                          </ul>}
                          hideSave={'hidden'}/>
                <PlanCard planSVG={<img className="m-auto" src="https://ik.imagekit.io/softylus/wordpress_hosting_Icon_wZkkygaMD0.svg"/>} header={"wordpress hosting"} price={9.99} btnLink={"/wordpress"}
                          myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                              <LI LI={"100% SSD cloud hosting"} textSize={"text-sm"}/>
                              <LI LI={"Unlimited bandwidth"} textSize={"text-sm"}/>
                              <LI LI={"WordPress optimised"} textSize={"text-sm"}/>
                              <LI LI={"Free SSL certificates"} textSize={"text-sm"}/>
                              <LI LI={"Daily backups"} textSize={"text-sm"}/>
                              <LI LI={"FREE website builder"} textSize={"text-sm"}/>
                          </ul>}
                          hideSave={'hidden'}/>
                <PlanCard planSVG={<img className="m-auto" src="https://ik.imagekit.io/softylus/shared_hosting_Icon_F0J3p5xwY1.svg"/>} header={"shared hosting"} price={3.99} btnLink={"/shared"}
                          myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                              <LI LI={"100% SSD cloud hosting"} textSize={"text-sm"}/>
                              <LI LI={"Unlimited bandwidth"} textSize={"text-sm"}/>
                              <LI LI={"WordPress optimised"} textSize={"text-sm"}/>
                              <LI LI={"Free SSL certificates"} textSize={"text-sm"}/>
                              <LI LI={"Daily backups"} textSize={"text-sm"}/>
                              <LI LI={"FREE website builder"} textSize={"text-sm"}/>
                          </ul>}
                          hideSave={'hidden'}/>
                <PlanCard planSVG={<img className="m-auto" src="https://ik.imagekit.io/softylus/Cloud_VPN_Icon_12XC4YT7Jx.svg"/>} hideSave={"hidden"} header={"cloud hosting"} price={5.99} btnLink={"/vps"}
                          myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside",listStyleSize:"2px"}} className="list-disc">
                              <LI LI={"100% SSD cloud hosting"} textSize={"text-sm"}/>
                              <LI LI={"Unlimited bandwidth"} textSize={"text-sm"}/>
                              <LI LI={"WordPress optimised"} textSize={"text-sm"}/>
                              <LI LI={"Free SSL certificates"} textSize={"text-sm"}/>
                              <LI LI={"Daily backups"} textSize={"text-sm"}/>
                              <LI LI={"FREE website builder"} textSize={"text-sm"}/>
                          </ul>}/>
            </div>
        </section>
        <section className="DomainChecker bg-blue-700">
            <div className="max-w-4xl mx-auto p-8 mobile:pb-3">
                <h2 className="text-2xl text-center font-bold text-white">Looking for your identity to be named?</h2>
                <h6 className="text-md mb-8 mt-2 text-center text-white">We&apos;ll generate it for you!</h6>
                <div className="mobile:my-4 my-8 bg-white grid grid-cols-5 mobile:grid-cols-4 p-1 mobile:p-0.5 border-2 border-gray-200 rounded-full">
                    <input     onInput={getDomainResults} className="outline-none mr-2 bg-none col-span-4 mobile:col-span-3 mobile:py-2 px-4 py-3  rounded-full" placeholder="Domain.com"/>
                   
                    <button onClick={getDomainResults} className="flex justify-center text-white font-bold  bg-black rounded-full mobile:text-sm mobile:px-8  py-3">
                        <svg className="mr-2 mobile:hidden" width="23" height="24" viewBox="0 0 23 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.1516 0.685449C4.72926 0.685449 0.317773 5.09584 0.317773 10.5169C0.317773 15.9384 4.72926 20.3484 10.1516 20.3484C15.5742 20.3484 19.9854 15.9384 19.9854 10.5169C19.9854 5.09584 15.5742 0.685449 10.1516 0.685449ZM10.1516 18.2611C5.88055 18.2611 2.40555 14.7869 2.40555 10.517C2.40555 6.24707 5.88055 2.7728 10.1516 2.7728C14.4226 2.7728 17.8976 6.24703 17.8976 10.5169C17.8976 14.7869 14.4226 18.2611 10.1516 18.2611Z" fill="white" stroke="white" strokeWidth="0.3"/>
                            <path d="M17.1759 16.0645L22.3008 21.1882C22.7087 21.5956 22.7087 22.2567 22.3008 22.6641C22.097 22.8678 21.8295 22.9698 21.5628 22.9698C21.2958 22.9698 21.0286 22.8678 20.8248 22.6641L15.6998 17.5403C15.6998 17.5403 15.6998 17.5403 15.6998 17.5403C15.292 17.1329 15.292 16.4719 15.6998 16.0645L17.1759 16.0645ZM17.1759 16.0645C16.7681 15.6568 16.1076 15.6568 15.6998 16.0644L17.1759 16.0645Z" fill="white" stroke="white" strokeWidth="0.3"/>
                        </svg>
                        Search
                    </button>
                </div>
              
                <div className=" flex justify-center">
                    <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-white font-bold ">.Com</span>
                    <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-white font-bold">.Co</span>
                    <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-white font-bold">.Net</span>
                    <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-white font-bold">.Org</span>
                    <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-white font-bold">.Club</span>
                    <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-white font-bold">.Design</span>
                    <span className="mobile:p-0.5 p-2  mx-0.5 text-sm text-white font-bold">.Xyz </span>
                </div>
            </div>
        </section>
            <Migration   image={data.mi.childImageSharp.fluid} />
             <Blazing  image={data.bl.childImageSharp.fluid}  />
        <section>
            <svg style={{margin: 'auto'}} width="60" height="40" viewBox="0 0 40 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M9.03163 0C14.1388 0 18.1713 4.02973 18.1713 8.9999C18.1713 13.9701 13.9768 17.9998 8.86971 17.9998C8.81862 17.9998 8.74155 17.9931 8.69047 17.9922C10.7098 19.5571 13.246 20.5001 16.0221 20.5001C17.2993 20.5001 18.3271 21.5079 18.3271 22.75C18.3271 23.9921 17.5054 25 16.2282 25C7.163 24.9992 0.000219345 17.822 0.000219345 8.9999C0.000219345 8.99821 0.000219345 8.99653 0.000219345 8.994C0.000219345 4.02636 3.92624 0 9.03163 0Z" fill="#D3D3D3" fillOpacity="0.5"/>
                <path d="M30.6915 0C35.7986 0 39.8381 4.02973 39.8381 8.9999C39.8381 13.9701 35.6471 17.9998 30.5391 17.9998C30.488 17.9998 30.4127 17.9931 30.3616 17.9922C32.3809 19.5571 34.918 20.5001 37.6941 20.5001C38.9713 20.5001 40 21.5079 40 22.75C40 23.9921 39.1653 25 37.8881 25C28.8229 24.9992 21.6471 17.822 21.6471 8.9999C21.6471 8.99821 21.6471 8.99653 21.6471 8.994C21.6471 4.02636 25.5861 0 30.6915 0Z" fill="#D3D3D3" fillOpacity="0.5"/>
            </svg>
             <h1 className="text-black text-2xl font-bold text-center">Clients Says About us</h1>
            <p  className="text-gray-400 text-lg text-center">Clients Reviews </p>

            <div style={{ padding: `0 ${chevronWidth}px` }}>
                <ItemsCarousel
                    requestToChangeActive={setActiveItemIndex}
                    activeItemIndex={activeItemIndex}
                    numberOfCards={4}
                    gutter={10}
                    infiniteLoop={true}
                    leftChevron={<svg className="w-8 h-8 mobile:w-4 mobile:h-4 text-gray-200" fill="none"
                                      stroke="currentColor"
                                      viewBox="0 0 24 24"
                                      xmlns="http://www.w3.org/2000/svg">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 19l-7-7 7-7"></path>
                    </svg>}
                    rightChevron={<svg className="w-8 h-8 mobile:w-4 mobile:h-4 text-gray-200" fill="none"
                                       stroke="currentColor"
                                       viewBox="0 0 24 24"
                                       xmlns="http://www.w3.org/2000/svg">
                        <path strokeLinecap="round" strokLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7"></path>
                    </svg>}
                    outsideChevron
                    chevronWidth={chevronWidth}
                >
                        <Slide name={"Ayoub Assouka"} message={"Our website is hosted with Hostylus since 2 months and they are rrally amazing! Highly recommended! Thanks!"} image={"https://i2.wp.com/hostylus.com/wp-content/uploads/2020/11/73x73.png?resize=73%2C73&ssl=1"}/>
                        <Slide name={"Joud Schools"} message={"Most of the hosting companies is for people who basically know what they're doing on the internet and on their websites. but Hostylus is for all types of people nevermind If you are a newbie or non-technical.The support team is super-nice, helpful and fast response. they tell you exactly what to do.the support staff speak very good English although they're not native English speakers.There are other site hosts that might cost another 5 or 10 dollars more per month.And they serve the same services.I have been with them for 1 and half year Definitely gona renew specially when the head of support team talked to me to ask how i am doing and if there is any issues in my website since I didn't call them for like 6 months. And he told me he will talk to his managers to give me a good offer.They did."} image={"https://hostylus.com/wp-content/uploads/2020/11/noprofile-06.svg"}/>
                        <Slide name={"Ibraheem Hatoqai"} message={"I’ve been benefiting from hostylus hosting service for 4 months, I never noticed any website downs and the speed is good from server end. Pricing is reasonable.Keep up the good job guys!"} image={"https://hostylus.com/wp-content/uploads/2020/11/73x73-1.png"}/>
                        <Slide name={"Laura Lopez"} message={"where do I start? literally, I tried many many hosting companies before I meet Hostylus and Softylus the guy how manages these companies is an expert and understands what his customer needs and working exactly to give us the value that we need.I'm super happy to be the first review here because he provides us much more than what we need."} image={"https://hostylus.com/wp-content/uploads/2020/11/noprofile-06.svg"}/>
                        <Slide name={"Abdulrhman Barham"} message={"What a nice company that serves the finest of services, got a hosting for 1 year and absolutely will renew it next year with them 😁"} image={"https://hostylus.com/wp-content/uploads/2020/11/73x73-2.png"}/>
                </ItemsCarousel>
            </div>
      </section>
    </Layout>
  );
}

export default IndexPage;


export const landing = graphql`
query MyQueryL {
    landing:file(relativePath: {eq: "Image_With_Features_TzbgBf09ceLr.png"}) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    bl:file(relativePath: {eq: "blazing_zWUltm8vE.png"}) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    mi:file(relativePath: {eq: "Image_wlaMgCBuN.png"}) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`  