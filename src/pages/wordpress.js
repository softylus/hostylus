import React, {useState} from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Landing from "../components/landengSection";
import PlanCard from "../components/plan_card";
import LI from "../components/plan-li";
import Feature from "../components/featureBlock";
import FeatureList from "../components/featureLists";
import Accordion from "../components/accordion"
import Img from "gatsby-image"
import { graphql } from "gatsby"

function wordpressHosting({data}) {
    const [Annualy, setCheck] = useState(true);
    function changeAnnualy(){
        setCheck(prevAnnualy => !prevAnnualy);
    }
    return (
        <Layout>
            <SEO
                keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
                title="Home"
            />
            <Landing smallHeader="More than content management system"
                     header="wordpress hosting"
                     desc="Boost your online reach without worrying about your website’s hosting plans. Our wide range of options will help you achieve your intended goals."
                     btnURL="/"
                     image={data.wordpress.childImageSharp.fluid}

            />
            <section className=" max-w-4xl mx-auto my-16 mobile:pb-3 ">
                <h2 className="text-4xl text-black font-bold text-center mobile:px-4 tablet:px-6">Upgraded plans today for a better tomorrow</h2>
                <h4 className="text-gray-400 text-center text-xl px-16 my-3">Unleash the technology of your mind, and elevate your business sales with our wordpress hosting plans</h4>
                <div className="grid grid-cols-7 mt-8 mobile:grid-cols-1">
                    <h3 className={`col-span-3 text-right mobile:text-center text-${Annualy ? 'black' : 'gray-400'} font-bold text-xl`}>Bill Annualy</h3>
                    <label className="px-6 mobile:text-center"><input id="s2" type="checkbox" className="switch" onClick={changeAnnualy}/></label>
                    <h3 className={`col-span-3 text-left mobile:text-center text-${Annualy ? 'gray-400' : 'black'} font-bold text-xl`}>Bill Monthly</h3>
                </div>
                <div className="grid grid-cols-3 gap-2 flex justify-items-center mt-6 mobile:grid-cols-1 tablet:grid-cols-1 py-8">
                    <PlanCard savings={17} header={"startup"} desc={"A basic plan to fit your startup needs"} price={Annualy ? 11.99 : 9.99} btnLink={"/"}
                              myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                                  <LI LI={"1 website"} textSize={"text-md"}/>
                                  <LI LI={"1000MB RAM"} textSize={"text-md"}/>
                                  <LI LI={"3 MySQL Databases" } textSize={"text-md"}/>
                                  <LI LI={"Unlimited Bandwidth"} textSize={"text-md"}/>
                                  <LI LI={"Free SSL"}/>
                                  <LI LI={"10 Emails Accounts"} textSize={"text-md"}/>
                                  <LI LI={"Free Migration"} textSize={"text-md"}/>
                                  <LI LI={"Free Domain"} textSize={"text-md"} Dell={"line-through"}/>
                              </ul>}
                              hideSave={Annualy ? '' : 'hidden'}/>
                    <PlanCard savings={11} header={"growing"} desc={"A basic plan to fit your startup needs"} price={Annualy ? 14.99 : 16.99} btnLink={"/"}
                              myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                                  <LI LI={"3 website"} textSize={"text-md"}/>
                                  <LI LI={"2000MB RAM"} textSize={"text-md"}/>
                                  <LI LI={"6 MySQL Databases"} textSize={"text-md"}/>
                                  <LI LI={"Unlimited Bandwidth"} textSize={"text-md"}/>
                                  <LI LI={"Free SSL"}/>
                                  <LI LI={"20 Emails Accounts"} textSize={"text-md"}/>
                                  <LI LI={"Free Migration"} textSize={"text-md"}/>
                                  <LI LI={"Free Domain"} textSize={"text-md"}/>
                              </ul>}
                              hideSave={Annualy ? '' : 'hidden'}/>
                    <PlanCard savings={11} header={"cruising"} desc={"A basic plan to fit your startup needs"} price={Annualy ? 19.99 : 21.99} btnLink={"/"}
                              myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                                  <LI LI={"Unlimited websites"} textSize={"text-md"}/>
                                  <LI LI={"3000MB RAM"} textSize={"text-md"}/>
                                  <LI LI={"Unlimited MySQL Databases"} textSize={"text-md"}/>
                                  <LI LI={"Unlimited Bandwidth"} textSize={"text-md"}/>
                                  <LI LI={"Free SSL"} textSize={"text-md"}/>
                                  <LI LI={"Unlimited Emails Accounts"} textSize={"text-md"}/>
                                  <LI LI={"Free Migration"} textSize={"text-md"}/>
                                  <LI LI={"Free Domain"} textSize={"text-md"}/>
                              </ul>}
                              hideSave={Annualy ? '' : 'hidden'}/>
                </div>
            </section>
            <section className="features max-w-6xl mx-auto my-16 mobile:px-3">
                <h2 className="text-4xl text-black font-bold text-center">your easiest wordpress solutions</h2>
                <div className="features grid grid-cols-3 gap-3 flex justify-items-stretch mt-6 mobile:grid-cols-1 tablet:grid-cols-2 py-8">
                    <Feature image={"https://ik.imagekit.io/gjojptnaqxc/wordpress_icon_pUC86E99X6.svg"}
                             header={"WordPress Simplified"}
                             desc={"One-click installer to initialize and configure WordPress from start to finish. One dashboard to mass-manage multiple WordPress instances."}
                    />
                    <Feature image={"https://ik.imagekit.io/gjojptnaqxc/secure_F394wP97ci.svg"}
                             header={"Secure Against Attacks"}
                             desc={"Hardens your site by default, further enhanced with the Toolkit’s No security expertisenecessary."}
                    />
                    <Feature image={"https://ik.imagekit.io/gjojptnaqxc/storage_OHjqtZZWHE.svg"}
                             header={"Stage and Test"}
                             desc={"One-click installer to initialize and configure WordPress from start to finish. One dashboard to mass-manage multiple WordPress instances."}
                    />
                    <Feature image={"https://ik.imagekit.io/gjojptnaqxc/automate_MUAciduIp0.svg"}
                             header={"Run and Automate"}
                             desc={"Singularly or mass-execute updates to the WP core, themes or plugins. Monitor and run all your WordPress sites from one dashboard."}
                    />
                    <Feature image={"https://ik.imagekit.io/gjojptnaqxc/complexity_GvA7sH6TBT.svg"}
                             header={"Cut Out Complexity"}
                             desc={"One-click installer to initialize and configure WordPress from start to finish. One dashboard to mass-manage multiple WordPress instances."}
                    />
                    <Feature image={"https://ik.imagekit.io/gjojptnaqxc/simple_2ID_6IocL7.svg"}
                             header={"Simple, but not Amateur"}
                             desc={"Get full control with WP-CLI, maintenance mode, debug management, search engine index management and more."}
                    />

                </div>
            </section>
            <section className="screens my-16 mobile:pb-3">
                <div className="flex mobile:flex-col-reverse tablet:flex-col-reverse my-32 d">
                    <div className="pl-32 laptop:pl-0 mobile:px-0.5 tablet:px-0.5 w-1/2 mobile:w-full tablet:w-full">
                        <h3 className="font-bold text-3xl px-16 mobile:text-center">Build</h3>
                        <FeatureList header="easy installation"
                                     desc="The one-click installer of WordPress Toolkit does everything from start to finish – downloads WP, creates a DB with a DB user, creates an admin account in WordPress and initializes WordPress so that it’s fully ready for use."
                        />
                        <FeatureList header="Staging Environment"
                                     desc="With WordPress Toolkit you can clone your site and set up a staging environment for your experiments, new features and new ideas. You can sync to production when you’re ready."
                        />
                        <FeatureList header="Theme and Plugin Management"
                                     desc="Quickly find and install a plugin or theme on a WordPress instance or several instances at once. Activate and deactivate plugins. Bulk remove plugins and themes that are not needed. Install plugin and theme sets on existing sites."
                        />
                    </div>
                    <div className=" w-1/2 mobile:w-full tablet:w-full">
                    <Img
                        fluid={data.build.childImageSharp.fluid}
                      alt="A corgi smiling happily"
                        />
                    {/* <img className="ml-2 float-right" src="https://ik.imagekit.io/softylus/browser_obsidian_content-manager_build_OxYgGTwwN.png"/> */}
                    </div>
                </div>


                <div className="flex justify-between mobile:grid tablet:grid my-32">
                    {/*<img src="https://ik.imagekit.io/softylus/image__2__Y7qhucs8m.png"/>*/}
                    <div className="text-left w-1/2 mobile:w-full tablet:w-full">
                        {/* <img className="mobile:mr-16" src="https://ik.imagekit.io/softylus/browser_obsidian_content-manager_secure_nAdckg49Y4.png"/> */}
                        <Img
                        fluid={data.secure.childImageSharp.fluid}
                      alt="A corgi smiling happily"
                        />
                
                        </div>
                    <div className="pr-16 laptop:pr-0 mobile:px-0.5 w-1/2 mobile:w-full tablet:w-full">
                        <h3 className="font-bold text-3xl px-16 mobile:text-center">Secure</h3>
                        <FeatureList header="Click Hardening"
                                     desc="Scan all WordPress sites with Plesk to identify and protect your core installations, no manual work needed. Simply check the items you wish to harden, click “Secure”, and you’re done."
                        />
                        <FeatureList header="No Security Expertise Needed"
                                     desc="WordPress Toolkit security scanner goes beyond the basics and implements the latest security recommendations and best practices from WP Codex and WP security experts."
                        />
                        <FeatureList header="Smart Updates"
                                     desc="Smart Updates analyzes your WordPress updates, identifies changes and decides whether to perform them, without breaking your production site. Stay updated to prevent security risks."
                        />
                    </div>
                </div>
                <div className="flex mobile:flex-col-reverse tablet:flex-col-reverse my-32">
                    <div className="pl-32 laptop:pl-0 mobile:px-0.5 tablet:px-0.5 w-1/2 mobile:w-full tablet:w-full">
                        <h3 className="font-bold text-3xl px-16 mobile:text-center">Run</h3>
                        <FeatureList header="Backup and Restore Points"
                                     desc="If something goes wrong, restore points and backups allow you to restore your site back to its previous state."
                        />
                        <FeatureList header="Debug Management"
                                     desc="Manage all important debug options in WordPress or manage debug options on a per-instance basis from a single interface."
                        />
                        <FeatureList header="Site Indexing"
                                     desc="Enable or disable search engines from indexing your site on a per-instance basis."
                        />
                        <FeatureList header="Maintenance Mode in WordPress Toolkit"
                                     desc="Activate WordPress maintenance mode when updating WordPress, plugins, or themes with a single click."
                        />
                        <FeatureList header="Command Line Interface"
                                     desc="Easily access WordPress Command Line Interface for all your WordPress instances. Import a database, create a new user, update themes and plugins using WP-CLI. Access a full range of cloning options with Cloning CLI."
                        />
                    </div>
                    <div className=" w-1/2 mobile:w-full tablet:w-full">
                    <Img
                        fluid={data.run.childImageSharp.fluid}
                      alt="A corgi smiling happily"
                        />
                        {/* <img className="ml-2 float-right" src="https://ik.imagekit.io/softylus/browser_obsidian_content-manager_run_MyHamMOOQ.png"/> */}
                        </div>
                    {/*<img className="" src="https://ik.imagekit.io/softylus/right_screen_Pvjrjqghg.png"/>*/}
                </div>
            </section>
            <section className="Epic h-96 py-16" >
                <div className="grid justify-items-center">
                    <h3 className="mobile:text-2xl text-center text-white text-4xl font-bold">What Makes Us Your Pick?</h3>
                    <p className="mobile:px-5 tablet:px-6 mobile:text-lg px-64 py-4 text-center text-white text-xl">We’re capable of giving you the full support you’ll need with your WordPress website from scratch with the help of our partner company that skillfully covers this part and more.</p>
                    {/* <img className="pt-8 mobile:px-2" src="https://ik.imagekit.io/gjojptnaqxc/epic_B50lgtIEs.png"/> */}
                </div>
            </section>
            <section className="acc py-16 grid justify-items-center">
                <div className=" px-8 max-w-2xl">
                    <h3 className="text-center text-black mb-16 text-4xl font-bold uppercase">Entery-level Let’s encrypt protection</h3>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"OCSP Stapling"} content={"Enhances the privacy of website’s visitors and improves the website performance. The web server will request the status of the website’s certificate (can be good, revoked, or unknown) from the CA instead of the visitor’s browser doing so."}/>
                    <Accordion title={"SSL Labs Test"} content={"Deep analysis of the SSL web server configuration."}/>
                    <Accordion title={"Keep websites secured"} content={"Automatically replaces expired or self-signed SSL/TLS certificates with free valid certificates from Let’s Encrypt. Covers each domain, subdomain, domain alias, and webmail belonging to the subscription."}/>
                </div>
            </section>

        </Layout>
    )
}
export default wordpressHosting;



export const wordpress = graphql`
query MyQuery2 {
    wordpress:file(relativePath: {eq: "Hostylus-Wordpress-Hosting.png"}) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    secure:file(relativePath: {eq: "secure.png"}) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      build:file(relativePath: {eq: "build.png"}) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      run:file(relativePath: {eq: "run.png"}) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
  }
`  