import React, {useState} from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Landing from "../components/landengSection";
// import AmazingPerVPS from "../components/AmazingPerVPS";
// import ExcelentServices from "../components/ExcelentServices";
// import GoodToGo from "../components/GoodToGo";
import FeaturesVPS from "../components/FeaturesVPS";
// import InfiniteCarousel from 'react-leaf-carousel';
import PricingVps from "../components/PricingVps"
import ImageLeft from "../components/imageLeft";
import ImageRight from "../components/imageRight";
import ItemsCarousel from "react-items-carousel";
import { graphql } from "gatsby"
/* eslint react/prop-types: 0 */
function vpsHosting({data}) {
    const [activeItemIndex, setActiveItemIndex] = useState(0);
    const chevronWidth = 60;
    return (
        <Layout>
            <SEO
                keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
                title="Home"
            />
            <Landing smallHeader=""
                     header="HOSTYLUS VPS HOSTING"
                     desc="MORE THAN WHAT YOU HAVE IN MIND, LESS THAN WHAT'S IN ACCOUNT!"
                     boldText="STARTING AT $5.99"
                     btnURL="/"
                     image={data.vps.childImageSharp.fluid}

            />
            <ImageRight header={"INCREDIBLE PERFORMANCE STARTING AT $5.99"}
                        desc={"Handle even your most resource-intensive projects with ease. Our cloud servers are located in  state-of-the-art data centers. You get best-in-class performance with AMD EPYC 2nd Gen, Intel® Xeon® Gold processors and speedy NVMe SSDs."}
                        image={data.per.childImageSharp.fluid}
                        hideShopNow={"hidden"}
            />

            <ImageLeft header={"GOOD TO GO IN SECONDS"}
                        desc={"Get started without waiting! With our friendly interface, you can create server instances almost instantly, usually in under 10 seconds that prevent any imposter from sneak peekingat your data"}
                        image={data.good.childImageSharp.fluid}
                        hideShopNow={"hidden"}
            />
 
            <ImageRight header={"EXCELLENT SERVICES"}
                        desc={"The Hostylus VPS hosting products are provided by provider whom won platinum at the Service Provider Awards 2020. Several thousand readers helped to choose the winner in a survey posted on several IT portals."}
                        image={data.excellent.childImageSharp.fluid}
                        hideShopNow={"hidden"}
            />
            <div className="w-4/5 m-auto mobile:w-full">

            <div style={{ padding: `0 ${chevronWidth}px` }}>
                <ItemsCarousel
                    requestToChangeActive={setActiveItemIndex}
                    activeItemIndex={activeItemIndex}
                    numberOfCards={4}
                    gutter={12}
                    slidesToScroll={1}
                    infiniteLoop={true}
                    leftChevron={<svg className="w-8 h-8 mobile:w-4 mobile:h-4 text-gray-500" fill="none"
                                      stroke="currentColor"
                                      viewBox="0 0 24 24"
                                      >
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 19l-7-7 7-7"></path>
                    </svg>}
                    rightChevron={<svg className="w-8 h-8 mobile:w-4 mobile:h-4 text-gray-500" fill="none"
                                       stroke="currentColor"
                                       viewBox="0 0 24 24"
                                      >
                        <path strokeLinecap="round" strokLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7"></path>
                    </svg>}
                    outsideChevron
                    chevronWidth={chevronWidth}
                >
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=43" target="_blank" rel="noreferrer">{ <PricingVps headerVPS={"HX11"} priceVPS={5.99} vCPU={1} RAM={2} diskSpace={20} traffic={20}/> }</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=44" target="_blank" rel="noreferrer">{<PricingVps  headerVPS={"HPX11"} priceVPS={8.10} vCPU={2} RAM={2} diskSpace={40} traffic={20}/>}</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=45" target="_blank" rel="noreferrer">{<PricingVps  headerVPS={"HX21"} priceVPS={10.30} vCPU={2} RAM={4} diskSpace={40} traffic={20}/>}</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=46" target="_blank" rel="noreferrer">{<PricingVps  headerVPS={"HPX21"} priceVPS={15.99} vCPU={3} RAM={4} diskSpace={80} traffic={20}/>}</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=47" target="_blank" rel="noreferrer">{<PricingVps  headerVPS={"HX31"} priceVPS={20.99} vCPU={2} RAM={8} diskSpace={80} traffic={20}/> }</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=48" target="_blank" rel="noreferrer">{ <PricingVps headerVPS={"HPX31"} priceVPS={29.99} vCPU={4} RAM={8} diskSpace={160} traffic={20}/> }</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=49" target="_blank" rel="noreferrer">{<PricingVps  headerVPS={"HX41"} priceVPS={36.99} vCPU={4} RAM={16} diskSpace={160} traffic={20}/>}</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=50" target="_blank" rel="noreferrer">{<PricingVps  headerVPS={"HPX41"} priceVPS={52.99} vCPU={8} RAM={16} diskSpace={240} traffic={20}/>}</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=51" target="_blank" rel="noreferrer">{<PricingVps  headerVPS={"HX51"} priceVPS={69.99} vCPU={8} RAM={32} diskSpace={240} traffic={20}/>}</a>
                  <a href="http://client.hostylus.com/cart.php?a=add&pid=52" target="_blank" rel="noreferrer">{<PricingVps  headerVPS={"HPX51"} priceVPS={119.99} vCPU={16} RAM={32} diskSpace={360} traffic={20}/> }</a>


                </ItemsCarousel>
            </div>


         <FeaturesVPS />
</div>
            </Layout> 
       )
    }

    export default vpsHosting;

    export const vps = graphql`
    query MyQueryVV {
        vps:file(relativePath: {eq: "landing_vps_hosting_98dwEk7zZ.png"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
        per:file(relativePath: {eq: "per.png"}) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      good:file(relativePath: {eq: "gtg.png"})  {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      excellent:file(relativePath: {eq: "exe.png"}) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          } 
        }
      }
    }
    `  