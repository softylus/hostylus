import React from "react";
import Slider from 'react-animated-slider';
import PropTypes from "prop-types";
import 'react-animated-slider/build/horizontal.css';
// import "../styles/slider-animations.css";
MySlider.propTypes = {
    title: PropTypes.string.isRequired, // must be a node and defined
    desc: PropTypes.string.isRequired, // must be a node and defined
};

export default function MySlider (props) {
    const content = [
        {
            title:props.title,
            description:props.desc,
        },
        {
            title:props.title,
            description:props.desc,
        },
        {
            title:props.title,
            description:props.desc,
        },
]
return(
    <section  className="myslider">
        <Slider className="slider px-16 mobile:px-0 h-72"
                autoplay={3000}
                previousButton={<svg className="w-8 h-8 mobile:w-4 mobile:h-4 text-black" fill="none"
                                     stroke="currentColor"
                                     viewBox="0 0 24 24"
                                     xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 19l-7-7 7-7"></path>
                </svg>}
                nextButton={<svg className="w-8 h-8 mobile:w-4 mobile:h-4 text-black" fill="none"
                                 stroke="currentColor"
                                 viewBox="0 0 24 24"
                                 xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7"></path>
                </svg>}
        >
            {content.map((item, index) => (
                <div
                    key={index}
                    className="slide h-64"
                >
                    <div className="px-32 mobile:px-6 h-full text-center">
                            <img className="w-12 mobile:w-8 my-6 mx-auto" src="https://ik.imagekit.io/softylus/blueTick_nzCxIZ4xA.svg"/>
                            <h1 className="pb-4 text-black text-2xl mobile:text-lg">{item.title}</h1>
                            <p className="py-1 text-gray-500 text-lg mobile:text-sm">{item.description}</p>
                    </div>
                </div>
            ))}
        </Slider>
    </section>
);
}