import React from "react";
import Img from "gatsby-image"
import PropTypes from "prop-types";
function imageRight(props) {
    return(
        <section>
            <div className="tablet:p-8  flex m-auto laptop:my-10 bg-white mobile:my-10 mobile:flex-col-reverse tablet:mt-16">



                <div className="w-1/2 pl-40 tablet:pl-0  mobile:pl-0 my-auto mobile:my-4 mobile:text-center tablet:align-bottom tablet:my-0 mobile:w-full tablet:w-full ">
                    <h1 className="  font-extrabold w-2/3 mobile:w-full mobile:mt-5 mobile:text-center tablet:text-xl text-3xl mb-3  ">{props.header}</h1>
                    <span className="uppercase font-bold text-gray-500">{props.smallHeader}</span>
                    <p className="font-light  my-3 mobile:w-full mobile:px-10   mobile:text-center">
                        {props.desc}</p>
                    <div className={` w-2/3 mobile:w-full mobile:text-center  ${props.hideLearnMore}`}>
                        <a href={props.learnMore} className="inline-block text-blue-600 font-black uppercase text-md underline mr-2">learn more</a>
                        <img className="inline-block" src="https://ik.imagekit.io/softylus/arrow_b_EEPW_gX.svg"/>
                    </div>
                    <div className={` w-2/3 mobile:w-full mobile:text-center my-6 ${props.hideShopNow}`}>
                        <a href={"/"} className="text-black-600 font-semibold uppercase text-md rounded-full border-2 border-black py-2 px-6 mr-2">Shop now</a>
                    </div>
                </div>
                <div className="w-1/2 mx-10 my-10 tablet:m-auto mobile:m-auto mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
                    {/* <img className="m-auto mobile:p-4" src={props.image}/> */}
                    <Img  className="m-auto mobile:p-4"
                       fluid={props.image}
                          alt="A corgi smiling happily"
                     />
                </div>
            </div>
        </section>

    );
}

export default imageRight;
imageRight.propTypes = {
    image:PropTypes.string.isRequired, // must be a string and defined
    header:PropTypes.string.isRequired, // must be a string and defined
    smallHeader:PropTypes.string,
    desc:PropTypes.string.isRequired, // must be a string and defined
    learnMore:PropTypes.string, // must be a string and defined
    hideLearnMore:PropTypes.string, // must be a string and defined
    hideShopNow:PropTypes.string, // must be a string and defined
};