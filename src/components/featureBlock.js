import React from "react";
import PropTypes from "prop-types";
function featureBlock(props){
    return(
        <div className="border-2 rounded-lg p-8">
            <img className="mx-auto" src={props.image}></img>
            <h2 className="text-center text-xl font-bold">{props.header}</h2>
            <p className="text-center text-md text-gray-500 py-3">{props.desc}</p>
        </div>
    );
}
export default featureBlock;
featureBlock.PropTypes={
    image: PropTypes.string.isRequired, // must be a string and defined
    header: PropTypes.string.isRequired, // must be a string and defined
    desc: PropTypes.string, // must be a string and defined
}