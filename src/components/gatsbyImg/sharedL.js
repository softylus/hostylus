import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.com/docs/use-static-query/
 */

const sharedL = () => {
  const data = useStaticQuery(graphql`
    query {
      blazing: file(relativePath: { eq: "solid_linux_d6CJdfO4z.png" }) {
        childImageSharp {
          fluid(maxWidth: 500, quality: 50) {
            ...GatsbyImageSharpFluid

          }
         
        }
      }
    }
  `)
  if (!data?.blazing?.childImageSharp?.fluid) {
    return <div>Picture not found</div>
  }
  return <Img fluid={data.blazing.childImageSharp.fluid} />
}

export default sharedL
